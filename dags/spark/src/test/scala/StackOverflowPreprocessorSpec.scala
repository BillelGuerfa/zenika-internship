import com.github.mrpowers.spark.fast.tests.DataFrameComparer
import org.apache.spark.sql.Row
import org.apache.spark.sql.types._
import org.scalatest.FunSpec

class StackOverflowPreprocessorSpec extends FunSpec with DataFrameComparer with SparkSessionTestWrapper {
    import spark.implicits._

    it("Removes urls from comments extracted text attribute and returns sentences"){
        val sourceDs = Seq(
            (26782616,"You probably need an `std::unique_ptr<Window>`, which you can reset to `new Barlett(100)` or whatever.","2013-08-15 20:50:58.947 UTC",18261411,661519,"",0),
            (84756596,"I recommend going through basic bash tutorial to get more comfortable with the command line. For example http://blog.teamtreehouse.com/introduction-to-the-mac-os-x-command-line","2018-02-20 00:44:48.13 UTC",48875597,7417402,"",0),
            (86208123,"You should try `xlwings` as per the answer: https://stackoverflow.com/a/49600540/4752883","2018-04-01 17:08:46.507 UTC",8973935,4752883,"",0),
            (32841962,"Thanks you very much! Works! Does that require admin on XP? Weird. At school works on some XP machines an all 7 machines. Do you know anything about this?","2014-02-12 03:53:55.063 UTC",21693743,3291472,"",0)
        ).toDF("id","text","creation_date","post_id","user_id","user_display_name","score").as[StackOverflowPreprocessor.Comment] 
        
        val actualDF = StackOverflowPreprocessor.commentProcessor(sourceDs).toDF("id","text","creation_date","post_id","user_id","user_display_name","score")
    
        val expectedDF =  Seq(
            (26782616,"You probably need an code snippet, which you can reset to code snippet or whatever.","2013-08-15 20:50:58.947 UTC",18261411,661519,"",0),
            (84756596,"I recommend going through basic bash tutorial to get more comfortable with the command line. For example ","2018-02-20 00:44:48.13 UTC",48875597,7417402,"",0),
            (86208123,"You should try code snippet as per the answer: ","2018-04-01 17:08:46.507 UTC",8973935,4752883,"",0),
            (32841962,"Thanks you very much! Works! Does that require admin on XP? Weird. At school works on some XP machines an all 7 machines. Do you know anything about this?","2014-02-12 03:53:55.063 UTC",21693743,3291472,"",0)
        ).toDF("id","text","creation_date","post_id","user_id","user_display_name","score")

        assertSmallDataFrameEquality(actualDF, expectedDF)
        
    }
    it ("Removes newlines from posts"){
        val sourceDs = Seq(
            (415503802,"""<p>Probably not. I don't know what version of Matplotlib you are using but at some point a bug existed with this issue that apparently still has repercussions for the version I'm using today, i.e. 1.5.1 (check <a href=""https://stackoverflow.com/questions/8971309/matplotlib-3d-scatter-color-lost-after-redraw"">this question</a> for more on this).</p>

            <p>Adapting @Yann solution to your problem should resolve that issue. I tested it in my computer and the result is the following:</p>
            
            <pre><code>import matplotlib.pyplot as plt
            from mpl_toolkits.mplot3d import Axes3D
            
            x = [3.28912855713, 3.31252670518]
            y = [4.64604863545, 4.65385917898]
            z = [3.95526335139, 3.96834118896]
            b2 = [1, 1]
            
            fig = plt.figure()
            ax = fig.add_subplot(111,projection='3d')
            line = ax.scatter(x, y ,z , c=b2, s=1500, marker='*', edgecolors='none', depthshade=0)
            cb = plt.colorbar(line)
            
            def forceUpdate(event):
                global line
                line.changed()
            
            fig.canvas.mpl_connect(""draw_event"", forceUpdate)
            
            plt.show()
            </code></pre>
            
            <p>Images of the plot at entry and after a rotation are:</p>
            
            <p><a href=""https://i.stack.imgur.com/kaXsE.png"" rel=""nofollow noreferrer""><img src=""https://i.stack.imgur.com/kaXsE.png"" alt=""3D matplotlib scatterplot with color""></a></p>
            
            <p><a href=""https://i.stack.imgur.com/CbLY0.png"" rel=""nofollow noreferrer""><img src=""https://i.stack.imgur.com/CbLY0.png"" alt=""3D matplotlib scatterplot with color after rotation""></a></p>"""",1,"","2013-03-19 15:34:54.153 UTC","2013-03-19 15:34:54.153 UTC","","","","",179910,15503537,2,3,"")
        ).toDF("id","body","comment_count","community_owned_date","creation_date","last_activity_date","last_edit_date","last_editor_display_name","last_editor_user_id","owner_display_name","owner_user_id","parent_id","post_type_id","score","tags").as[StackOverflowPreprocessor.Answer] 
        
        val actualDF = StackOverflowPreprocessor.answerProcessor(sourceDs).toDF("id","body","comment_count","community_owned_date","creation_date","last_activity_date","last_edit_date","last_editor_display_name","last_editor_user_id","owner_display_name","owner_user_id","parent_id","post_type_id","score","tags")
    
        val expectedDF =  Seq((415503802,""" Probably not. I don't know what version of Matplotlib you are using but at some point a bug existed with this issue that apparently still has repercussions for the version I'm using today, i.e. 1.5.1 (check this question for more on this). Adapting @Yann solution to your problem should resolve that issue. I tested it in my computer and the result is the following: code snippet Images of the plot at entry and after a rotation are: """,1,"","2013-03-19 15:34:54.153 UTC","2013-03-19 15:34:54.153 UTC","","","","",179910,15503537,2,3,""))
                        .toDF("id","body","comment_count","community_owned_date","creation_date","last_activity_date","last_edit_date","last_editor_display_name","last_editor_user_id","owner_display_name","owner_user_id","parent_id","post_type_id","score","tags")

        assertSmallDataFrameEquality(actualDF, expectedDF)
    }
    
}