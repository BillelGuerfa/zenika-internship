
import org.apache.spark.sql.{Dataset, SaveMode, SparkSession}
import org.apache.spark.sql.functions._
import com.databricks.spark.xml._

import scala.util.matching.Regex
import scala.annotation.implicitNotFound


object StackOverflowPreprocessor extends Serializable {
  case class Comment(id: String, post_id: String, score: String, text: String, creation_date: String, user_id: String, user_display_name: String)
  case class Answer(id: String,
                    body: String,
                    comment_count: String,
                    community_owned_date: String,
                    creation_date: String,
                    last_activity_date: String,
                    last_edit_date: String,
                    last_editor_display_name: String,
                    last_editor_user_id: String,
                    owner_display_name: String,
                    owner_user_id: String,
                    parent_id: String,
                    post_type_id: String,
                    score: String,
                    tags: String)
  case class Question(id: String,
                      title: String,
                      body: String,
                      accepted_answer_id: String,
                      answer_count: String,
                      comment_count: String,
                      community_owned_date: String,
                      creation_date: String,
                      favorite_count: String,
                      last_activity_date: String,
                      last_edit_date: String,
                      last_editor_display_name: String,
                      last_editor_user_id: String,
                      owner_display_name: String,
                      owner_user_id: String,
                      post_type_id: String,
                      score: String,
                      tags: String,
                      view_count: String)
  val spark = SparkSession.builder.getOrCreate()


  def unescape_chars(s: String): String ={
    new String(s.getBytes("utf-8"), "utf-8")
  }

  def commentProcessor (ds: Dataset[Comment]) : Dataset[Comment] = {

    import spark.implicits._
    val unescape_chars_UDF = udf[String, String](unescape_chars)
    var commentsDS = ds.withColumn("text", unescape_chars_UDF(col("text"))).as[Comment]
                        .withColumn("text", regexp_replace(col("text"), """(https?:\/\/)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/=]*)""", "")).as[Comment]
                        .withColumn("text", regexp_replace(col("text"), """(`.*?`)|(<code>.*?<\/code>)""", "code snippet")).as[Comment]
                        .withColumn("text", regexp_replace(col("text"), """\n""", "")).as[Comment]
    commentsDS
  }

  def answerProcessor (ds: Dataset[Answer]) : Dataset[Answer] = {
    import spark.implicits._
    val unescape_chars_UDF = udf[String, String](unescape_chars)
    var answersDS = ds.withColumn("body", unescape_chars_UDF(col("body"))).as[Answer]
      .withColumn("body", regexp_replace(col("body"), """[\r\n]+""", "")).as[Answer]
    //.withColumn("body", regexp_replace(col("body"), """([ \t]{2,})""", " ")).as[Answer]
    .withColumn("body", regexp_replace(col("body"), """([ \t]{2,})""", " ")).as[Answer]
    .withColumn("body", regexp_replace(col("body"), """(`.*?`)|(<code>.*?<\/code>)""", "code snippet")).as[Answer]
    .withColumn("body", regexp_replace(col("body"), """<.*?>""", " ")).as[Answer]
    .withColumn("body", regexp_replace(col("body"), """(https?:\/\/)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&\/=]*)""", "")).as[Answer]
    
    .withColumn("body", regexp_replace(col("body"), """["]""", "")).as[Answer]
    .withColumn("body", regexp_replace(col("body"), """([ \t]{2,})""", " ")).as[Answer]
    //.withColumn("body", regexp_replace(col("body"), """(^.*$)""", "\"$1\"")).as[Answer]
    answersDS
  
  }
  def run (t: String, filePath: String, outputFilePath: String) {
    import spark.implicits._
    //var i = 0

    t match {
      case "Comments" => 
        var commentsDS = spark.read.format("csv")
                          .option("header", "true")
                          .option("sep", ",")
                          .option("multiLine", "true")
                          .load(filePath)
                          .as[Comment]
        var comments = commentProcessor(commentsDS)
        comments.show
        comments.select("text").write.format("csv").mode(SaveMode.Overwrite).csv(outputFilePath)
        spark.stop()

      case "Answers" => 
        /*var answersDS = spark.read.format("csv")
                        .option("header", "true")
                        .option("sep", ",")
                        .option("multiLine", "true")
                        .load(filePath)
                        .as[Answer]*/
        var answersDS = spark.read.json(filePath).as[Answer]
        var answers = answerProcessor(answersDS)
        answers.select("body").write.format("csv").mode(SaveMode.Overwrite).csv(outputFilePath)
        //answers.withColumn("body",col("body")).write.format("csv").mode(SaveMode.Overwrite).csv(outputFilePath)
        answers.select("body").show
        //answers.select("body").show
        spark.stop()

    }
      
    
    
    // while (i < 10000000) {
      
    //   comments = commentProcessor(commentsDF)
    //   i = i + 1;
    // }

  }
}


object Main{
  def main(args: Array[String]): Unit = {
    println(args)
    if(args.length == 3) StackOverflowPreprocessor.run(args(0), args(1), args(2))
  }
}