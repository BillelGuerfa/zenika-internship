locals {
  cluster_type = "simple-regional"
}


locals {
  subnet_01 = "${var.vpc_network}-subnet-01"
  subnet_02 = "${var.vpc_network}-subnet-02"
}

module "vpc" {
  source       = "terraform-google-modules/network/google"
  project_id   = "${var.project_id}"
  network_name = "${var.vpc_network}"
  routing_mode = "REGIONAL"

  subnets = [
    {
      subnet_name   = "${local.subnet_01}"
      subnet_ip     = "10.10.0.0/24"
      subnet_region = "${var.gcp_region}"
    }
  ]

  secondary_ranges = {
    "${local.subnet_01}" = []
  }
}

resource "google_compute_address" "ip_address" {
  name = "ingress-ip"
  network_tier = "PREMIUM"
  project = "${var.project_id}"
}

module "gke" {
  source            = "terraform-google-modules/kubernetes-engine/google"
  project_id        = "${var.project_id}"
  name              = "${local.cluster_type}-cluster${var.cluster_name_suffix}"
  regional          = false
  region            = "${var.gcp_region}"
  zones             = "${var.gcp_zones}"
  network           = "${module.vpc.network_name}"
  subnetwork        = "${module.vpc.subnets_names[0]}"
  ip_range_pods     = ""
  ip_range_services = ""
  horizontal_pod_autoscaling = true
  kubernetes_dashboard       = true
  network_policy             = true
  remove_default_node_pool          = true
  disable_legacy_metadata_endpoints = false

   node_pools = [
    {
      name               = "pool-01"
      machine_type       = "n1-standard-4"
      min_count          = 1
      max_count          = 50
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      service_account    = "174323558749-compute@developer.gserviceaccount.com"
      preemptible        = false
      initial_node_count = 1
      
    },
    {
      name               = "pool-02"
      machine_type       = "n1-standard-8"
      min_count          = 0
      max_count          = 50
      disk_size_gb       = 100
      disk_type          = "pd-standard"
      image_type         = "COS"
      auto_repair        = true
      auto_upgrade       = true
      service_account    = "174323558749-compute@developer.gserviceaccount.com"
      preemptible        = true
      initial_node_count = 0
    }
  ]

  node_pools_labels = {
    all = {}

    pool-01 = {}

    pool-02 = {}
  }
  node_pools_oauth_scopes = {
    all = ["https://www.googleapis.com/auth/cloud-platform"]

    pool-01 = []

    pool-02 = []
  }
  node_pools_taints = {
    all = []

    pool-01 = []

    pool-02 = []
  }

  node_pools_tags = {
    all = []

    pool-01 = []

    pool-02 = []
  }
  node_pools_metadata = {
    all = {}

    pool-01 = {}

    pool-02 = {}
  }
}

data "google_client_config" "default" {}