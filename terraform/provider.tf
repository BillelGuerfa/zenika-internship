provider "aws" {
    access_key = "${var.aws_access_key}"
    secret_key = "${var.aws_secret_key}"
    region = "${var.aws_region}"
}
provider "google" {
  version = "~> 2.7.0"
  region  = "${var.gcp_region}"
  credentials = "${file("credentials.json")}"

}

provider "google-beta" {
  version = "~> 2.7.0"
  region  = "${var.gcp_region}"
  credentials = "${file("credentials.json")}"
}
