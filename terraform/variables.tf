
variable "project_id" {
    default = "billel-internship"  
}
variable "vpc_name" {
    default = "billel-internship-vpc"
}

variable "cluster_name" {
    default = "billel-internship"
}
variable "gcp_region" {
    default = "us-west1"
}
variable "gcp_zones" {
   type = "list"
   default = ["us-west1-b"]

}
variable "cluster_name_suffix" {
  description = "A suffix to append to the default cluster name"
  default     = ""
}

variable "vpc_network" {
  description = "The VPC network to host the cluster in"
  default = "kubernetes-vpc"
}

variable "vpc_subnetwork" {
  description = "The subnetwork to host the cluster in"
  default = "kubernetes-subnet"
}

variable "ip_range_pods" {
  description = "The secondary ip range to use for pods"
  default = "10.200.0.0/16"
}

variable "ip_range_services" {
  description = "The secondary ip range to use for pods"
  default = ""
}

variable "compute_engine_service_account" {
  description = "Service account to associate to the nodes in the cluster"
  default = "174659178379-compute@developer.gserviceaccount.com"
}
variable "aws_region" {
    default = "us-east-1"
}
variable "aws_access_key" {}
variable "aws_secret_key" {}