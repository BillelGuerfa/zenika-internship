resource "aws_emr_cluster" "cluster" {
    name = "${var.cluster_name}"
    release_label = "${var.release_label}"
    applications  = ["Spark"]

    termination_protection = false
    keep_job_flow_alive_when_no_steps = false

    service_role = "EMR_DefaultRole"
    
    ec2_attributes {
        instance_profile = "${var.emr_instance_profile}"
    }

    instance_group {
        instance_role = "MASTER"
        instance_type = "${var.master_instance_type}"
        instance_count = "1"
        bid_price = "${var.master_bid_price}"
    }
    instance_group {
        instance_role = "CORE"
        instance_type = "${var.core_instance_type}"
        instance_count = "${var.core_instance_count}"
        bid_price = "${var.core_bid_price}"
    }
}
