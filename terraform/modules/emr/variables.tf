variable "cluster_name" {
}
variable "release_label" {
    default = "emr-5.23.0"
}
variable "master_instance_type" {
}
variable "master_bid_price" {
}
variable "core_instance_type" {
}
variable "core_instance_count" {
}
variable "core_bid_price" {
}
variable "emr_instance_profile" {
    default = "EMR_EC2_DefaultRole"
  
}


