gcloud container clusters get-credentials simple-regional-cluster --region us-west1-b
helm init
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/influxdb/heapster.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/influxdb/influxdb.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/rbac/heapster-rbac.yaml
kubectl apply -f gke-admin-service-account.yaml

gcloud container clusters get-credentials simple-regional-cluster --zone us-west1-b --project billel-internship
gcloud config config-helper --format=json | jq .credential.access_token
