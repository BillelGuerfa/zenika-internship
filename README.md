# ZenTechInsight

## Repository structure

This repository contains the following :

- `Kubernetes` : This directory contains all kubernetes ressources (deployments, pods, volumes, services...) needed by the project according to the architecture, those are mostly yaml files.

- `dags` : This directory contains Airflow ETL pipeline artifacts, dag python files and spark scala jobs in the subdirectory spark.

- `terraform` : This directory contains the deployment part of the project, terraform files for deploying AWS EKS cluster, S3 bucket and every cloud ressource needed by the project.

- `ci` : The ci directory contains all ressources used by gitlab-ci pipline.

## Project

This project is an infrastructure for a sentiment analysis application on stack-overflow posts and comments, it is useful for getting some insight on a developpement technology before testing it, if you're hesitating between two great technologies and you have to make a choice.

The project is called ZenTechInsight, as it will be used inside the company Zenika by the consultants to explore various technologies.

As a data engineer, I will talk about how to architect this project and how to deploy it on AWS.

First, I will talk about the big components of the project in terms of Data sources, Storage, Data Processing and Machine learning, then I will introduce the technologies that can achieve what we need, finally, I will explain the archetecture and it's advantages against other possibilities.

### Data Sources

Stack overflow publishes all their data in a public archive repository every three months, it contains all the stack-exchange network data, what we need is the posts, comments, and tags of stack-overflow, they are available as semi-structured XML files (containing fields + text).

### Storage

- `S3:` The size of the stack-overflow data is ~100 GB, Amazon S3 is perfect for storing this data as it is, we need raw data to pre-process it using Spark,as we will need every sentence in a line on a text file for the Word2vec algorithm (aws BlazingText).

- `MongoDB:` We also need MongoDB as we will do tag based search and also some basic full text search on the data by a webservice used by the application, the data will be ingested into MongoDB as part of the ETL process.

### Data Processing

We process the raw XML data using a Scala spark job, the job takes the posts and the comments, removes URLs and special chars (and maybe stemming ?), and outputs every sentence on a line in a single txt file.

### Machine Learning

There are two parts here, the first one is to build our word vectors using Word2vec, or BlazingText in this case (Amazon's scallable Word2vec implementation on Sagemaker), in the second part, we classify the developer's sentiment of each post/comment using Senti4D sentiment analysis Gold Standard of ~4500 labelised stack overflow posts by real developers.

## Architecture
`//TODO`

![Architecture](./architecture.png)
