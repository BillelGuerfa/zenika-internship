from datetime import timedelta

import airflow
from airflow.models import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.dummy_operator import DummyOperator



default_args = {
    'owner': 'airflow',
    'retries': 0,
    'start_date': airflow.utils.dates.days_ago(2)
    }


dag = DAG(dag_id='infinite_loop_dag', default_args=default_args, schedule_interval='0 0 * * *')


infinite = BashOperator(
    task_id='infinite',
    bash_command="while true; do echo 'Hit CTRL+C'; sleep 1; done",
    dag=dag,
)


infinite