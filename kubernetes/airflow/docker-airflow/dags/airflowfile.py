from datetime import timedelta

import airflow
from airflow.models import DAG
from airflow.contrib.operators.spark_submit_operator import SparkSubmitOperator
#from libs.spark_submit_operator import SparkSubmitOperator
from airflow.operators.dummy_operator import DummyOperator


default_args = {
    'owner': 'airflow',
    'retries': 0,
    'start_date': airflow.utils.dates.days_ago(2)
    }

dag = DAG(dag_id='internship_pipeline', default_args=default_args, schedule_interval='0 0 * * *')

run_this_first = DummyOperator(
    task_id='run_this_first',
    dag=dag,
)


clean_so_data = SparkSubmitOperator(
    task_id='clean_so_data',
    application="local:///jobs/StackOverflowPreprocessor-assembly-0.1.0-SNAPSHOT.jar",
    name = "clean_so_data",
    num_executors = 3,
    conn_id="spark",
    java_class = "Main",
    conf={
        "spark.executor.instances" : 3,
        "spark.kubernetes.container.image":"gcr.io/billel-internship/spark",
        "spark.kubernetes.container.image.pullPolicy":"Always",
        "spark.kubernetes.authenticate.driver.serviceAccountName" : "spark"
    },
    dag=dag
)

run_this_first >> clean_so_data