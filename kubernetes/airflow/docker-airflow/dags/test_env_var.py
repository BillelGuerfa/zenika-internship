import time
from pprint import pprint

import airflow
from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.models.connection import Connection
from airflow.hooks.base_hook import BaseHook

args = {
    'owner': 'airflow',
    'start_date': airflow.utils.dates.days_ago(1),
}

dag = DAG(
    dag_id='test_conn_env',
    default_args=args,
    schedule_interval=None,
)

CONN_ENV_PREFIX = "AIRFLOW_CONN_"
def get_connection_from_env(conn_id):
    while(True):
        print(BaseHook("test").get_connection(conn_id))
    return BaseHook("test").get_connection(conn_id)

run_this = PythonOperator(
    task_id='print_env_conn',
    python_callable= get_connection_from_env,
    op_kwargs={"conn_id":"spark"},
    dag=dag,
)

run_this