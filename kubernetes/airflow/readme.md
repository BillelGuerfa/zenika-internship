# Airflow running on Kubernetes

**Note:** First the Airflow Docker image needs to be on the local registery

```bash
git clone https://github.com/BillelGuerfa/airflow.git

./airflow/scripts/ci/kubernetes/docker/build.sh
```

This directory contains the following:

- `postgres.yaml:` The deployment of a postgres pod, with an emptyDir volume and a service exposing it on port 5432, only accessible from within the cluster.
- `airflow.yaml:` This is the deployment of the airflow scheduler and webserver, + service accessible from outside the cluster + PersistentVolumeClaims for dags and logs.
- `configmaps.yaml:` Airflow configuration  (the executor is KubernetesExecutor).
- `secrets.yaml:` Postgres database credentials for airflow.
- `volumes.yaml:` Persistent volumes used by Airflow.
- `deploy.sh:` a bash script running the sequence of kubectl apply to deploy the whole stack.