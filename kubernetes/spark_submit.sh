airflow/docker-airflow/spark-2.4.2-bin-hadoop2.7/bin/spark-submit --master k8s://https://35.199.173.168 --deploy-mode cluster --name spark-test \
--conf spark.kubernetes.namespace=default \
--conf spark.executor.instances=2 \
--class "Main" \
--conf spark.kubernetes.container.image=gcr.io/billel-internship/spark \
--conf spark.kubernetes.container.image.pullPolicy=Always \
--conf spark.hadoop.google.cloud.auth.service.account.enable=true \
--conf spark.hadoop.google.cloud.auth.service.account.json.keyfile=/mnt/secrets/spark-sa.json \
local:///jobs/StackOverflowPreprocessor-assembly-0.1.0-SNAPSHOT.jar
